import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent{
@Input() FormGroup:any;
@Input() FormControlName:any;
@Input() FormArray:any=[];

}
