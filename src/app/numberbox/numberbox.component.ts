import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-numberbox',
  templateUrl: './numberbox.component.html',
  styleUrls: ['./numberbox.component.css']
})
export class NumberboxComponent implements OnChanges {
@Input() FormGroup:any;
@Input() FormControlName:any;
@Input() placeholder:any;

squarefeet:any=[{
  india:10000000000000
},{
  america:23456779
},{
  australia:7643456
}]

ngOnChanges(){
for (let obj of this.squarefeet) {
  if(Object.keys(obj)[0]===this.FormGroup.value.countryName)
  this.FormControlName.value=obj.this.FormGroup.value.countryName;
}
}
}
