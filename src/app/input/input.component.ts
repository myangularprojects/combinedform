import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent {
@Input() FormControlName:any;
@Input() FormGroup:any;
@Input() placeholder:any;

inputChange(){
  this.FormGroup=this.FormGroup;
}
}
