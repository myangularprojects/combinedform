import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css']
})
export class CountryDetailsComponent {
form:any;
constructor(private fb:FormBuilder){
  this.form=this.fb.group({

    countryName:[''],
    capital:[''],
    totalArea:[''],
    internationalOrganizations:[''],
    states:['']
  })
}

countriesAndState:any=[
  {
    india:["tamilnadu","andhra","telangana","karnataka","kerala","karnataka"]
  },{
    america:["los angeles","new york","mexico"]
  },{
    australia:["melbourne","brisbane","adelaide"]
  }
];

parts:any;
ngOnChanges(){
  console.log(this.form);
  for (let obj of this.countriesAndState) {
    if(Object.keys(obj)[0]===this.form.value.countryName)
    this.parts=obj.this.form.value.countryName;
    this.parts=[...this.parts];
  }
}

  printForm(){
    console.log(this.form)
  }

}
